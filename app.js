var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');

var app = express();
app.use(cors());
app.use(bodyParser.json()); // support json encoded bodies
//app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.post('/login', function (req, res) {
  var input = req.body; 
  if(input.userName == 'r2d2'){
    var loginResponse={
    authToken:'1234qweee',
    user:{
      username:'User1',
      roles:'admin',
      email:'abc@xyz.com'
    }   
  }
    res.status(200).json({data:loginResponse});
  }
  else{
    res.status(400).json({status:'failed',msg:'i failed because i can'});
  }
})


app.get('/intdata/get', function (req, res) {
  let list=[1,2,3,4,5,6];
   res.status(200).json({data:list});
   //res.status(400).json({status:'failed',msg:'i failed because i can'});
})

app.get('/stringdata/get', function (req, res) {
  let list=['a','b','c','d'];
   res.status(200).json({data:list});
   //res.status(400).json({status:'failed',msg:'i failed because i can'});
})

app.get('/devices', function (req, res) {
  let list=[
    {label:'a',value:'aa'},
    {label:'b',value:'bb'},
    {label:'c',value:'cc'}
  ];
   res.status(200).json({data:list});
   //res.status(400).json({status:'failed',msg:'i failed because i can'});
})

var server = app.listen(8081, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Sample app listening at http://%s:%s", host, port)

})